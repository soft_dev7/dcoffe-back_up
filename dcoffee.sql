--
-- File generated with SQLiteStudio v3.3.3 on อา. ก.ย. 11 17:43:47 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE category (
    category_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    category_name TEXT (50) NOT NULL
);

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         1,
                         'coffee'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         2,
                         'Dessert'
                     );


-- Table: Product
DROP TABLE IF EXISTS Product;

CREATE TABLE Product (
    product_id    INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name  TEXT (50) UNIQUE,
    product_price DOUBLE    NOT NULL,
    product_size  TEXT (5)  DEFAULT SML
                            NOT NULL,
    product_level TEXT (5)  DEFAULT (123) 
                            NOT NULL,
    product_type  TEXT (5)  NOT NULL
                            DEFAULT HCF,
    category_id   INTEGER   DEFAULT (12) 
                            REFERENCES category (category_id) ON DELETE RESTRICT
                                                              ON UPDATE RESTRICT
);

INSERT INTO Product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        1,
                        'Esspresso',
                        30.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO Product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        2,
                        'Americano',
                        40.0,
                        'SML',
                        '012',
                        'HC',
                        1
                    );

INSERT INTO Product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        3,
                        'Chiffon chocolate cake',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO Product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        4,
                        'Butter cake',
                        '-',
                        '-',
                        '-',
                        '-',
                        2
                    );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
